#include<linux/init.h>
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<asm/uaccess.h>
#include<linux/semaphore.h>
#include<linux/cdev.h>
#include<linux/proc_fs.h>
#include<linux/sched.h>
#include<linux/kthread.h>
#include<linux/time.h>
#include<linux/timer.h>

#define MAX_SIZE 100
#define PROCESS 9 //now any number of processes can be kept until computer doesnt gives up.
unsigned long execution_time[PROCESS];
struct task_struct *t1[PROCESS];
unsigned long sum;
int num_process=PROCESS;
static struct proc_dir_entry *task;
int i=0,k=0;
bool flag1=true;
char proc_data[MAX_SIZE];
int proc_data_int[PROCESS];
	
int exec_func(int a, struct timespec *start, struct timespec *end,struct timespec *s1, struct timespec *arrival);
int get_sum_of_exec_time();
void *f1();
void create_new_proc_entry();
int task_func(struct file *file, const char *buf, int count, void *data);

static int stopwatch_init(void){
	create_new_proc_entry();
	printk(KERN_INFO "FLAG: %d\n",flag1);
	int data=0;
	pid_t t12;
	for(i=0;i<num_process;i++){
		t1[i]=kthread_run(&f1,(void *)data,"FSSD");
		t12=t1[i]->pid;
		printk(KERN_INFO "pid: %d\n",t12);
		//t1[i]->state=TASK_RUNNING;
		t12=t1[i]->pid;
		printk(KERN_INFO "1:pid: %d\n\n",t12);
	}
	while(k<PROCESS);
	int as=0;
	printk(KERN_INFO "i: %d\n",i);
	printk(KERN_INFO "k: %d\n",k);
	int s=get_sum_of_exec_time();
	printk(KERN_INFO "************************");
	printk(KERN_INFO "TOTAL EXECUTION TIME: %d\n",s);
	int t=s/4;
	printk(KERN_INFO "TOTAL THROUGHPUT: %d\n",t);
	printk(KERN_INFO "************************");
}
/*
	------------------------------------------------------------
*/
void create_new_proc_entry(){
	task=create_proc_entry("f1",0666,NULL);
	if(!task)
	{
		printk("ERROR CREATING ASSIGN_TASK_PROC\n");
	}
	task->write_proc=task_func;
	printk("ASSIGN_TASK_PROC INTIALIZED\n");
}

int task_func(struct file *file, const char *buf, int count, void *data){
	printk("INSIDE ASSIGN_TASK_WRITE\n");
	printk("%d \n",count);
	if(count>MAX_SIZE)
		count=MAX_SIZE;
	if(copy_from_user(proc_data, buf, count))
		printk(KERN_INFO "EFAULT ERROR in COPY_FROM_USER\n");
	printk(KERN_INFO "HELLO\n");
	proc_data_int[0]=proc_data[0]-'0';
	printk(KERN_INFO "HELXXX\n");
	
	num_process=proc_data_int[0];
	printk(KERN_INFO "number of processes: %d\n",num_process);
	flag1=false;
	printk(KERN_INFO "flag=FALSE\n");
}
/*
	-------------------------------------------------------------
*/
void *f1(){
	struct timespec begin, end, diff, mid, arr;
	int a;
	a=k;
	if(exec_func(a,&begin, &end, &mid, &arr)!=0)
		printk("ERROR");
	
	printk(KERN_INFO "PROCESS %d\n",k);
	printk(KERN_INFO "\tARRIVAL TIME: %lu\n",arr.tv_nsec);
	printk(KERN_INFO "\tEXECUTION TIME: %lu\n",begin.tv_nsec);
	printk(KERN_INFO "\tFIRST OUTPUT TIME: %lu\n",mid.tv_nsec);
	printk(KERN_INFO "\tFINISH TIME: %lu\n",end.tv_nsec);
	diff=timespec_sub(begin,arr);
	printk(KERN_INFO "\tWAIT TIME: %lu\n",diff.tv_nsec);
	
	diff=timespec_sub(mid, arr);
	printk(KERN_INFO "\tRESPONSE TIME: %lu\n",diff.tv_nsec);
		
	diff=timespec_sub(end, begin);
	printk(KERN_INFO "\tRUN TIME: %lu\n",diff.tv_nsec);
		
	diff=timespec_sub(end, arr);
	printk(KERN_INFO "\tTURNAROUND TIME: %lu\n",diff.tv_nsec);
	
	if(diff.tv_sec >0){
		printk(KERN_INFO "diff.tv_sec>0");
		printk(KERN_INFO "%lu, %lu \n", diff.tv_sec, diff.tv_nsec);
		//execution_time[k]=diff.tv_nsec;
		//printk(KERN_INFO "exec time: %lu\n\n", execution_time[k]);
		k++;
	}
	else{
		printk(KERN_INFO "time taken by 9 empty loops of each 10^6: ");
		printk(KERN_INFO "%lu ns\n", diff.tv_nsec);
		//execution_time[k]=diff.tv_nsec;
		//printk(KERN_INFO "exec time: %lu\n\n", execution_time[k]);
		k++;
	}
	return 0;
}

int get_sum_of_exec_time(){
	int c=0;
	for(c=0;c<4;c++){
		sum += execution_time[c];
	}
	return sum;
}

int exec_func(int a,struct timespec *start, struct timespec *end,struct timespec *s1, struct timespec *arrival){
	getnstimeofday(arrival);
	int x=0,y=0;
	int p,q,r=0;
	p=a;
	q=a*10;
	getnstimeofday(start);
	for(x=0;x<p;x++){
		for(y=0;y<p;y++){
			r=p+q;
			if(r>0)
				getnstimeofday(s1);
			//printk(KERN_INFO "r: %d\n",r);
		}
	}
	getnstimeofday(end);
	return 0;
}
static void stopwatch_exit(void){
	printk("\nEXITING\n");
}

module_init(stopwatch_init);
module_exit(stopwatch_exit);
