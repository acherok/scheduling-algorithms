#include<linux/module.h>
#include<linux/time.h>

int exec_func(struct timespec *start, struct timespec *end, struct timespec *s1, struct timespec *arrival);

static int stopwatch_init(void){
	int i;
	struct timespec begin, end, mid, arr, diff;
	for(i=0;i<10;i++){
		if(exec_func(&begin, &end, &mid, &arr)!=0)
			printk("ERROR");
		
		printk(KERN_INFO "PROCESS %d\n",i);
		printk(KERN_INFO "\tARRIVAL TIME: %lu\n",arr.tv_nsec);
		printk(KERN_INFO "\tEXECUTION TIME: %lu\n",begin.tv_nsec);
		printk(KERN_INFO "\tFIRST OUTPUT TIME: %lu\n",mid.tv_nsec);
		printk(KERN_INFO "\tFINISH TIME: %lu\n",end.tv_nsec);
		diff=timespec_sub(begin,arr);
		printk(KERN_INFO "\tWAIT TIME: %lu\n",diff.tv_nsec);
		
		diff=timespec_sub(mid, arr);
		printk(KERN_INFO "\tRESPONSE TIME: %lu\n",diff.tv_nsec);
		
		diff=timespec_sub(end, begin);
		printk(KERN_INFO "\tRUN TIME: %lu\n",diff.tv_nsec);
		
		diff=timespec_sub(end, arr);
		printk(KERN_INFO "\tTURNAROUND TIME: %lu\n",diff.tv_nsec);
		
		//printk(KERN_INFO "begin time: %lu \n",begin.tv_nsec);
		//printk(KERN_INFO "mid time: %lu \n\n",mid.tv_nsec);
		//diff=timespec_sub(end,begin);
		
		if(diff.tv_sec >0){
			printk("diff.tv_sec>0");
			printk("%lu, %lu \n\n", diff.tv_sec, diff.tv_nsec);
		}
		else{
			printk("time taken by 9 empty loops of each 10^6: ");
			printk("%lu ns\n\n", diff.tv_nsec);
		}
	}
	return 0;
}
/*
	arrival, $execution, first output, $finish
*/
int exec_func(struct timespec *start, struct timespec *end,struct timespec *s1, struct timespec *arrival){
	//arrival
	getnstimeofday(arrival);
	unsigned int i;
	//execution
	getnstimeofday(start);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
	//first output - assume
	getnstimeofday(s1);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
		for (i = 0; i < 1000000; ++i);
	//finish
	getnstimeofday(end);
	return 0;
}
static void stopwatch_exit(void){
	printk("\nEXITING\n");
}

module_init(stopwatch_init);
module_exit(stopwatch_exit);











