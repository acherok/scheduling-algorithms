#include <linux/module.h>
#include <linux/time.h> /* timespec */


void stopwatch (char msg[32], int repeats, int (*function)( struct timespec *ts_start, struct timespec *ts_end));
static int stopwatch_init(void);
int ORIGINAL (struct timespec *ts_start, struct timespec *ts_end);
int PROPOSED (struct timespec *ts_start, struct timespec *ts_end);
static void stopwatch_exit(void);

void stopwatch (char msg[32], int repeats, int (*function)( struct timespec *, struct timespec *))
{
struct timespec begin, end, diff;
int i = 0;

printk ("%s", msg);

for (i = repeats; i > 0; i--){

	if (function( &begin, &end ) != 0)
		printk("Error");

	diff = timespec_sub(end, begin);

	if (diff.tv_sec > 0){
		printk("diff.tv_sec>0");
		printk ("%lu, %lu \n", diff.tv_sec, diff.tv_nsec );
	}
	else{
		printk("else");
		printk ("%lu \n", diff.tv_nsec );
	}
}

}

static int stopwatch_init(void)
{
printk ("Starting stopwatch...\n");

stopwatch("Proposed_code:, ", 10, &PROPOSED);
stopwatch("\nOriginal_code:, ", 10, &ORIGINAL);

return 0;
}

int ORIGINAL (struct timespec *ts_start, struct timespec *ts_end)
{
unsigned int i;

getnstimeofday (ts_start); /*stopwatch start*/

for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);


getnstimeofday (ts_end); /*stopwatch stop*/
return 0;
}

int PROPOSED (struct timespec *ts_start, struct timespec *ts_end)
{
unsigned int i;

getnstimeofday (ts_start); /*stopwatch start*/
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);
for (i = 0; i < 1000000; ++i);

getnstimeofday (ts_end); /*stopwatch stop*/
return 0;
}

static void stopwatch_exit(void)
{

	printk ("Exiting...\n");
}

module_init(stopwatch_init);
module_exit(stopwatch_exit);
