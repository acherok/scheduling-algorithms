#include<linux/module.h>
#include<linux/time.h>
#include<linux/init.h>
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/kthread.h>
#include<linux/sched.h>
 // required for various structures related to files liked fops.
 // required for copy_from and copy_to user functions
#include <linux/semaphore.h>
#include <linux/cdev.h>
#define threads 3
#define slice 2
struct task_struct *task[threads];
bool slice_flag=false; //flag not set.
bool c_flag=true;
int t_u_e;
struct semaphore sem;

void *f1();
int exec_func(int x,struct timespec *b,struct timespec *e,struct timespec *a,struct timespec *s);

int stopwatch_init(void){
	int data=0;
	//pid_t pid;
	int kop;
	int i=0;
	for(i=0;i<threads;i++){
		t_u_e=i;
		task[i]=kthread_run(&f1,(void*)data, "FSEFS");
		printk(KERN_INFO "THREAD %d STATE: %d\n",i,task[i]->state);
		for(kop=0;kop<1000000;kop++);
	}
	printk("\n\n");
	
	if(slice_flag=true){
		printk("XXXXXXXXXXXXXXXXX");
		//task[t_u_e]->state=TASK_INTERRUPTIBLE;
		printk("YYYYYYYYYYYYYYYYYYY");
		printk("thread id: %d\n",t_u_e);
		slice_flag=false;
	}
	//i make use of flags to sleep<->wakeup
	/*int j=0,o=0;
	while(c_flag==true){
		if(slice_flag==true){
			task[j]->state=TASK_STOPPED;
			printk(KERN_INFO"THREAD %d STOPPED\n",j);
			slice_flag=false;
			j++;
		}
		else{		
			printk(KERN_INFO"THREAD %d going to RUN\n",j);
			task[j]->state=TASK_RUNNING;
			t_u_e=j;
			printk(KERN_INFO"THREAD %d Running\n",j);
		}
		if(j>=3)
			c_flag=false;
	}*/
}

void *f1(){
	struct timespec b,e,a,s,d,d1;
	if(exec_func(t_u_e,&b,&e,&a,&s)!=0)
		printk(KERN_INFO "ERROR\n");
	
	d=timespec_sub(e,b);
	d1=timespec_sub(s,b);		
	
	printk(KERN_INFO "EXECUTION TIME: %lu\n",d.tv_nsec);
	printk(KERN_INFO "SLICED AT: %lu\n",d1.tv_nsec);
	/*if(d1.tv_nsec>slice)
		slice_flag=true;*/
}

int exec_func(int x,struct timespec *b,struct timespec *e,struct timespec *a,struct timespec *s){
	getnstimeofday(a);
	int w,e1;
	getnstimeofday(b);
	for(w=0;w<100000;w++);
	for(w=0;w<100000;w++);
			getnstimeofday(s);
			//slice_flag=true;
			if(t_u_e==1)
				task[t_u_e]->state=TASK_INTERRUPTIBLE;
	for(w=0;w<10000000;w++);
	for(w=0;w<100000;w++);
	for(w=0;w<100000;w++);
	getnstimeofday(e);
	return 0;
}
static void stopwatch_exit(void){
	printk("\nEXITING\n");
}

module_init(stopwatch_init);
module_exit(stopwatch_exit);
